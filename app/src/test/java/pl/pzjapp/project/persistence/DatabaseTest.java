package pl.pzjapp.project.persistence;

import android.arch.persistence.room.Room;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.List;

import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.dao.FavouriteDao;
import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Weather;

@RunWith(RobolectricTestRunner.class)
public class DatabaseTest {
    private CityDao cityDao;
    private WeatherDao weatherDao;
    private FavouriteDao favouriteDao;
    private TestUtils testUtils = new TestUtils();

    @Before
    public void set() {
        TheAppDatabase theAppDatabaseBuilder = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.systemContext, TheAppDatabase.class).allowMainThreadQueries().build();
        cityDao = theAppDatabaseBuilder.getCityDao();
        weatherDao = theAppDatabaseBuilder.getWeatherDao();
        favouriteDao = theAppDatabaseBuilder.getFavouriteDao();
        favouriteDao.insertFavourite(testUtils.createFavourite(1));
        favouriteDao.insertFavourite(testUtils.createFavourite(2));
        cityDao.insertCities(testUtils.createCity(3));
        weatherDao.insertWeather(testUtils.createWeather());
    }


    @Test
    public void cityTest() {
        Assert.assertNotNull("City not in db", cityDao.getCityById(10));
    }

    @Test
    public void weatherTest() {
        for (Weather weather : weatherDao.getWeathers())
            System.out.println(weather.toString());
        Assert.assertNotNull("weather not in db", weatherDao.getWeatherByDt(12345));
    }

    @Test
    public void relationCityWeather() {
        Assert.assertNotNull("City for Weather not found.", weatherDao.getWeatherForCity(10));
    }

    @Test
    public void favouriteTest() {
        Assert.assertNotNull("Favourite not found", favouriteDao.getFavouriteByName("testFavourite1"));
    }

    @Test
    public void checkFavouriteCityRelation() {
        cityDao.insert(new City(22, "test", "test", 2));
        cityDao.insert(new City(23, "test", "test", 1));
        List<City> citiesByFavouriteId = cityDao.getCitiesForFavourite(2);
        Assert.assertEquals(1, citiesByFavouriteId.size());
    }


}
