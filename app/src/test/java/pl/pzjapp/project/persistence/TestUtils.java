package pl.pzjapp.project.persistence;

import java.util.ArrayList;
import java.util.List;

import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Favourite;
import pl.pzjapp.project.persistence.model.Weather;

class TestUtils {
    private static final long CITY_ID = 10;

    Weather createWeather() {
        System.out.println("Create Weather");
        Weather weather = new Weather();
        weather.setWeatherId("weatherId");
        weather.setDate("weatherDate");
        weather.setType("weatherType");
        weather.setIconRef("weatherRef");
        weather.setDescription("weatherDescription");
        weather.setTemperature(10L);
        weather.setHumidity(10);
        weather.setPressure(10);
        weather.setWindSpeed(10L);
        weather.setDt(12345);
        weather.setCityId(CITY_ID);
        return weather;
    }

    Favourite createFavourite(long favouriteId) {
        System.out.println("createFavourite");
        return new Favourite(favouriteId, "testFavourite" + favouriteId);
    }

    List<City> createCity(int numberOfCities) {
        System.out.println("createCity");
        List<City> cityList = new ArrayList<>();
        for (long i = 0; i <= numberOfCities; ++i) {
            cityList.add(new City(CITY_ID + i, "testName" + i, "testCountry" + i, 1));
        }
        return cityList;
    }

}
