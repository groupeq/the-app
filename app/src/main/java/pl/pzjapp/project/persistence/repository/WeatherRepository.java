package pl.pzjapp.project.persistence.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.annotations.NonNull;
import pl.pzjapp.project.persistence.TheAppDatabase;
import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.persistence.tasks.GetWeatherForCityTask;
import pl.pzjapp.project.persistence.tasks.InsertWeatherTask;

public class WeatherRepository {
    private WeatherDao weatherDao;

    public WeatherRepository(@NonNull Context context) {
        this.weatherDao = TheAppDatabase.getInstance(context).getWeatherDao();
    }

    public void insertWeather(Long cityId, List<Weather> weathers) {
        for (Weather weatherToCheck : weathers) {
            try {
                if (!isWeatherInDatabase(cityId, weatherToCheck.getDt())) {
                    new InsertWeatherTask(weatherDao).execute(weatherToCheck);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<Weather> getWeathers() {
        return weatherDao.getWeathers();
    }

//    public LiveData<List<Weather>> getWeathersForCityByName(String name) {
//        return weatherDao.getWeathersForCityByName(name);
//    }

    public LiveData<List<Weather>> getLiveDataWeathers() {
        return weatherDao.getAllWeathers();
    }

    private boolean isWeatherInDatabase(final Long cityId, final long weatherDateTime) throws InterruptedException, ExecutionException, TimeoutException {
        List<Weather> weatherList = getWeathers(cityId);

        for (Weather weather : weatherList) {
            if (weather.getDt() == weatherDateTime)
                return true;
        }
        return false;
    }

    private List<Weather> getWeathers(Long cityId) throws ExecutionException, InterruptedException, TimeoutException {
        return new GetWeatherForCityTask(weatherDao)
                .execute(cityId)
                .get(1, TimeUnit.MINUTES);
    }

}
