package pl.pzjapp.project.persistence.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.pzjapp.project.persistence.TheAppDatabase;
import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.tasks.GetCitiesTask;
import pl.pzjapp.project.persistence.tasks.InsertCityTask;

public class CityRepository {

    private CityDao cityDao;

    public CityRepository(Context context) {
        cityDao = TheAppDatabase.getInstance(context).getCityDao();
    }

    public LiveData<City> getCityById(final long index) {
        return cityDao.getLiveCityById(index);
    }

    public LiveData<City> getCityByName(String cityName) {
        return cityDao.getCityByName(cityName);
    }

    public LiveData<List<City>> getAllCities() {
        return cityDao.getAllCities();
    }

    public LiveData<List<City>> getAllCitiesForPattern(String pattern) {
        pattern += "%";
        return cityDao.getAllCitiesForPattern(pattern);
    }

    public void insertCity(City city) {
        new InsertCityTask(cityDao).execute(city);
    }

    public boolean isCityTableEmpty() throws Exception {
        List<City> cities = new GetCitiesTask(cityDao)
                .execute()
                .get(1, TimeUnit.MINUTES);

        return cities.size() == 0;
    }

    public LiveData<List<City>> getCitiesForFavourite(long favouriteId) {
        return cityDao.getCitiesByFavouriteId(favouriteId);
    }

    public void updateFavouriteIdForCity(String cityName) {
        LiveData<City> cityByName = cityDao.getCityByName(cityName);
        City city = cityByName.getValue();
        if (city != null) {
            city.setFavouriteId(2);
            cityDao.update(city);
        }
    }
}
