package pl.pzjapp.project.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Favourite;

@Dao
public abstract class FavouriteDao {

    @Insert
    public abstract void insertFavourite(Favourite favourite);

    @Insert
    public abstract void insertFavourites(List<Favourite> favouriteList);

    @Update
    public abstract void updateFavourite(Favourite favourite);

    @Delete
    abstract void delete(Favourite favourite);

    @Query("SELECT * FROM favourite WHERE favourite_name=:name")
    public abstract Favourite getFavouriteByName(String name);

    @Query("SELECT * FROM favourite")
    public abstract LiveData<List<Favourite>> getAllFavourites();

    @Query("SELECT * FROM favourite")
    public abstract List<Favourite> getFavouriteList();

}
