package pl.pzjapp.project.persistence.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;

import pl.pzjapp.project.persistence.TheAppDatabase;
import pl.pzjapp.project.persistence.dao.FavouriteDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Favourite;
import pl.pzjapp.project.persistence.tasks.InsertFavouriteTask;
import pl.pzjapp.project.persistence.tasks.UpdateFavouriteTask;

public class FavouriteRepository {
    private FavouriteDao favouriteDao;

    public FavouriteRepository(Context context) {
        this.favouriteDao = TheAppDatabase.getInstance(context).getFavouriteDao();
    }


    public Favourite getFavouriteByName(String name) {
        return favouriteDao.getFavouriteByName(name);
    }


    public LiveData<List<Favourite>> getAllFavourites() {
        return favouriteDao.getAllFavourites();
    }

    public void insertFavourite(Favourite favourite) {
        new InsertFavouriteTask(favouriteDao).execute(favourite);
    }

    public void updateFavourite(Favourite favourite){
        new UpdateFavouriteTask(favouriteDao).execute(favourite);
    }

}
