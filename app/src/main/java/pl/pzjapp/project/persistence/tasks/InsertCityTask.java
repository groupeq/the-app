package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import io.reactivex.annotations.NonNull;
import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.utils.LogUtil;

public class InsertCityTask extends AsyncTask<City, Void, Void> {

    private CityDao cityDao;

    public InsertCityTask(@NonNull CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override
    protected Void doInBackground(City... city) {
        cityDao.insert(city[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LogUtil.task("Finished InsertCityTask execution.");
        super.onPostExecute(aVoid);
    }
}
