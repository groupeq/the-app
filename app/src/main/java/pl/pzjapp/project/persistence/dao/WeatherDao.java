package pl.pzjapp.project.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.pzjapp.project.persistence.model.Weather;

@Dao
public abstract class WeatherDao {
    @Insert
    public abstract void insertListOfWeathers(List<Weather> weather);

    @Insert
    public abstract void insertWeather(Weather weather);

    @Update
    public abstract void update(Weather weather);

    @Delete
    public abstract void delete(Weather weather);

    @Query("SELECT * FROM weather")
    public abstract LiveData<List<Weather>> getAllWeathers();

    @Query("SELECT * FROM weather")
    public abstract List<Weather> getWeathers();

    @Query("SELECT * FROM weather WHERE city_id = :cityId")
    public abstract List<Weather> getWeathersForCityById(long cityId);

    @Query("SELECT * FROM weather WHERE dt= :dt")
    public abstract Weather getWeatherByDt(long dt);

    @Query("SELECT * FROM weather WHERE city_id = :cityId")
    public abstract List<Weather> getWeatherForCity(long cityId);
}
