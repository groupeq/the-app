package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import java.util.List;

import pl.pzjapp.project.persistence.TheAppDatabase;
import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.utils.LogUtil;

public class InsertCitiesTask extends AsyncTask<List<City>, Void, Void> {
    private CityDao cityDao;

    public InsertCitiesTask(TheAppDatabase theAppDatabase) {
        this.cityDao = theAppDatabase.getCityDao();
    }

    @Override
    protected Void doInBackground(List<City>... lists) {
        cityDao.insertCities(lists[0]);
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        LogUtil.task("Finished InsertCitiesTask execution.");
        super.onPostExecute(aVoid);
    }
}
