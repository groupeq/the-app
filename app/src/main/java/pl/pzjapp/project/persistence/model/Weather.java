package pl.pzjapp.project.persistence.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Objects;

import io.reactivex.annotations.NonNull;

@Entity(tableName = "weather",
        indices = {@Index("id"), @Index("date"), @Index("city_id")},
        foreignKeys = @ForeignKey(entity = City.class, onDelete = ForeignKey.CASCADE, childColumns = "city_id", parentColumns = "city_id")
)
public class Weather {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;
    @NonNull
    @ColumnInfo(name = "date")
    private String date;
    @NonNull
    @ColumnInfo(name = "weather_id")
    private String weatherId;
    @NonNull
    @ColumnInfo(name = "type")
    private String type;
    @NonNull
    @ColumnInfo(name = "description")
    private String description;
    @NonNull
    @ColumnInfo(name = "icon_ref")
    private String iconRef;
    @NonNull
    @ColumnInfo(name = "wind_speed")
    private float windSpeed;
    @NonNull
    @ColumnInfo(name = "pressure")
    private float pressure;
    @NonNull
    @ColumnInfo(name = "humidty")
    private int humidity;
    @NonNull
    @ColumnInfo(name = "temperature")
    private float temperature;
    @NonNull
    @ColumnInfo(name = "city_id")
    private long cityId;
    @NonNull
    @ColumnInfo(name = "dt")
    private long dt;

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIconRef() {
        return iconRef;
    }

    public void setIconRef(String iconRef) {
        this.iconRef = iconRef;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public String getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(String weatherId) {
        this.weatherId = weatherId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weather weather = (Weather) o;
        return
                Float.compare(weather.windSpeed, windSpeed) == 0 &&
                        Float.compare(weather.pressure, pressure) == 0 &&
                        Float.compare(weather.humidity, humidity) == 0 &&
                        Float.compare(weather.temperature, temperature) == 0 &&
                        Objects.equals(date, weather.date) &&
                        Objects.equals(weatherId, weather.weatherId) &&
                        Objects.equals(type, weather.type) &&
                        Objects.equals(description, weather.description) &&
                        Objects.equals(iconRef, weather.iconRef) &&
                        Objects.equals(cityId, weather.cityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, weatherId, type, description, iconRef, windSpeed, pressure, humidity, temperature);
    }

    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", weatherId='" + weatherId + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", iconRef='" + iconRef + '\'' +
                ", windSpeed=" + windSpeed +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", temperature=" + temperature +
                ", dt=" + dt +
                ", city_id=" + cityId +
                '}';
    }
}
