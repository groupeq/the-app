package pl.pzjapp.project.persistence;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;

import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.dao.FavouriteDao;
import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Favourite;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.persistence.tasks.InitFavouriteTask;
import pl.pzjapp.project.persistence.tasks.InsertCitiesTask;
import pl.pzjapp.project.utils.AppUtils;
import pl.pzjapp.project.utils.JsonFileImporter;
import pl.pzjapp.project.utils.LogUtil;

@Database(entities = {City.class, Favourite.class, Weather.class}, version = 26, exportSchema = false)
public abstract class TheAppDatabase extends RoomDatabase {

    private static final AppUtils CONFIG = AppUtils.fromTestConfigFile();
    private static final String DB_NAME = CONFIG.get("db.name");
    private static TheAppDatabase DB_INSTANCE = null;

    public static void destroyInstance() {
        DB_INSTANCE = null;
    }

    public abstract CityDao getCityDao();

    public abstract FavouriteDao getFavouriteDao();

    public abstract WeatherDao getWeatherDao();

    public static TheAppDatabase getInstance(final Context context) {
        if (DB_INSTANCE == null) {
            synchronized (TheAppDatabase.class) {
                DB_INSTANCE = Room.databaseBuilder(context, TheAppDatabase.class, DB_NAME)
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build();
            }
        }
        return DB_INSTANCE;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            List<City> cityDataFromJsonFile = JsonFileImporter.getCityDataFromJsonFile();
            assert cityDataFromJsonFile != null;
            if (new InitFavouriteTask(DB_INSTANCE).execute(new Favourite(1, "System")).getStatus() == AsyncTask.Status.FINISHED)
                new InsertCitiesTask(DB_INSTANCE).execute(cityDataFromJsonFile);

        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            LogUtil.debug("Opening database");
        }
    };


}
