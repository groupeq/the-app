package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import io.reactivex.annotations.NonNull;
import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.utils.LogUtil;

public class InsertWeatherTask extends AsyncTask<Weather, Void, Boolean> {
    private WeatherDao weatherDao;

    public InsertWeatherTask(@NonNull WeatherDao weatherDao) {
        this.weatherDao = weatherDao;
    }

    @Override
    protected Boolean doInBackground(Weather... weather) {
        weatherDao.insertWeather(weather[0]);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        LogUtil.task("Finished InsertWeatherTask execution.");
        super.onPostExecute(aBoolean);
    }
}
