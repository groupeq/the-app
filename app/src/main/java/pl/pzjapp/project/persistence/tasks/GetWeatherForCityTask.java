package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import java.util.List;

import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.utils.LogUtil;

public class GetWeatherForCityTask extends AsyncTask<Long, Void, List<Weather>> {
    private WeatherDao weatherDao;

    public GetWeatherForCityTask(WeatherDao weatherDao) {
        this.weatherDao = weatherDao;
    }

    @Override
    protected List<Weather> doInBackground(Long... cityId) {
        return weatherDao.getWeathersForCityById(cityId[0]);
    }

    @Override
    protected void onPostExecute(List<Weather> weathers) {
        LogUtil.task("Finished GetWeatherForCityTask execution.");
        super.onPostExecute(weathers);
    }
}
