package pl.pzjapp.project.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.pzjapp.project.persistence.model.City;

@Dao
public abstract class CityDao {

    @Insert
    public abstract void insert(City city);

    @Update
    public abstract void update(City city);

    @Delete
    public abstract void delete(City city);

    @Query("SELECT * FROM city")
    public abstract LiveData<List<City>> getAllCities();

    @Query("SELECT * FROM city WHERE city_id=:cityId")
    public abstract LiveData<City> getLiveCityById(long cityId);

    @Query("SELECT * FROM city WHERE city_id= :cityId")
    public abstract City getCityById(long cityId);

    @Query("SELECT * FROM city WHERE city_name= :cityName LIMIT 1")
    public abstract LiveData<City> getCityByName(String cityName);

    @Query("SELECT * FROM city WHERE city_name LIKE :pattern LIMIT 20")
    public abstract LiveData<List<City>> getAllCitiesForPattern(String pattern);

    @Insert
    public abstract void insertCities(List<City> cityList);

    @Query("SELECT * FROM city")
    public abstract List<City> getListOfCities();

    @Query("SELECT * FROM city INNER JOIN favourite ON favourite.favourite_id = city.favourite_id WHERE city.favourite_id= :favouriteId")
    public abstract LiveData<List<City>> getCitiesByFavouriteId(long favouriteId);

    @Query("SELECT * FROM city INNER JOIN favourite ON favourite.favourite_id = city.favourite_id WHERE city.favourite_id = :favouriteId")
    public abstract List<City> getCitiesForFavourite(long favouriteId);

}

