package pl.pzjapp.project.persistence.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

@Entity(tableName = "favourite",
        indices = @Index("favourite_id"))

public class Favourite implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "favourite_id")
    private long id;
    @ColumnInfo(name = "favourite_name")
    private String name;

    @Ignore
    public Favourite(String name) {
        this.name = name;
    }

    public Favourite(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Favourite favourite = (Favourite) o;
        return id == favourite.id &&
                Objects.equals(name, favourite.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Favourite{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
