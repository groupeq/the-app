package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import pl.pzjapp.project.persistence.dao.FavouriteDao;
import pl.pzjapp.project.persistence.model.Favourite;
import pl.pzjapp.project.utils.LogUtil;

public class UpdateFavouriteTask extends AsyncTask<Favourite, Void, Void> {
    private FavouriteDao favouriteDao;
    public UpdateFavouriteTask(FavouriteDao favouriteDao) {
        this.favouriteDao = favouriteDao;
    }

    @Override
    protected Void doInBackground(Favourite... favourite) {
        favouriteDao.updateFavourite(favourite[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LogUtil.task("Finished UpdateFavouriteTask execution.");
        super.onPostExecute(aVoid);
    }
}
