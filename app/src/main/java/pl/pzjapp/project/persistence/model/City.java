package pl.pzjapp.project.persistence.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "city",
        indices = {@Index("city_id"), @Index("favourite_id")}, foreignKeys = @ForeignKey(
        entity = Favourite.class,
        parentColumns = "favourite_id",
        childColumns = "favourite_id",
        onDelete = CASCADE))
public class City implements Serializable {

    @ColumnInfo(name = "city_id")
    @PrimaryKey
    private long cityId;
    @ColumnInfo(name = "city_name")
    private String cityName;
    @ColumnInfo(name = "country")
    private String country;
    @ColumnInfo(name = "favourite_id")
    private long favouriteId;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getCityId() {
        return cityId;
    }

    public long getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(long favouriteId) {
        this.favouriteId = favouriteId;
    }

    public City() {
    }

    @Ignore
    public City(long cityId, String cityName, String country, long favouriteId) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.country = country;
        this.favouriteId = favouriteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return cityId == city.cityId &&
                Objects.equals(cityName, city.cityName) &&
                Objects.equals(country, city.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName, country, cityId);
    }

    @Override
    public String toString() {
        return "WeatherRepository{" +
                "cityName='" + cityName + '\'' +
                ", country='" + country + '\'' +
                ", cityId=" + cityId +
                ", favouriteId=" + favouriteId +
                '}';
    }
}
