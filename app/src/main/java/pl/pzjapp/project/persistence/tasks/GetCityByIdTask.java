package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.model.City;

public class GetCityByIdTask extends AsyncTask<Long, Void, City> {

    private final CityDao cityDao;

    public GetCityByIdTask(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override
    protected City doInBackground(Long... cityIds) {
        return cityDao.getCityById(cityIds[0]);
    }

    @Override
    protected void onPostExecute(City city) {
        super.onPostExecute(city);
    }
}
