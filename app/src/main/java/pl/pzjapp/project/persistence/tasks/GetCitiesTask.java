package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import java.util.List;

import pl.pzjapp.project.persistence.dao.CityDao;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.utils.LogUtil;

public class GetCitiesTask extends AsyncTask<Void, Void, List<City>> {
    private CityDao cityDao;

    public GetCitiesTask(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override
    protected List<City> doInBackground(Void... voids) {
        LogUtil.task("Finished GetCitiesTask execution.");
        return cityDao.getListOfCities();
    }
}
