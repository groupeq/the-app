package pl.pzjapp.project.persistence.tasks;

import android.os.AsyncTask;

import java.util.List;

import io.reactivex.annotations.NonNull;
import pl.pzjapp.project.persistence.dao.WeatherDao;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.utils.LogUtil;

public class InsertWeathersTask extends AsyncTask<List<Weather>, Void, Void> {
    private WeatherDao weatherDao;

    public InsertWeathersTask(@NonNull WeatherDao weatherDao) {
        this.weatherDao = weatherDao;
    }

    @Override
    protected Void doInBackground(List<Weather>... weathers) {
        weatherDao.insertListOfWeathers(weathers[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LogUtil.task("Finished InsertWeathersTask execution.");
        super.onPostExecute(aVoid);
    }
}
