package pl.pzjapp.project.utils;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.pzjapp.project.model.tool.ModelCreator;
import pl.pzjapp.project.persistence.model.Weather;

public class WeatherDownloader extends AsyncTask<Long, Void, List<Weather>> {
    private static final AppUtils CONFIG = AppUtils.fromTestConfigFile();
    private static final String API_KEY = CONFIG.get("api.key");
    private static final String API_URL = CONFIG.get("api.url");

    /**
     * This async method opens an HttpURLConnection with OpenWeatherMap API, reads JSON document
     * and calls parseJSON function from utils to assign data to ArrayList
     *
     * @return null
     */
    @Override
    protected List<Weather> doInBackground(Long... id) {
        ModelCreator modelCreator = new ModelCreator();
        HttpJsonParser httpJsonParser = new HttpJsonParser();
        JSONObject apiObject;
        List<Weather> weatherList = new ArrayList<>();
        for (Long cityId : id) {
            apiObject = httpJsonParser.makeHttpRequest(getUrl(cityId), "GET", null);
            try {
                weatherList = modelCreator.createWeather(apiObject, id[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return weatherList;
    }

    private String getUrl(Long aLong) {
        return API_URL + aLong +
                "&units=metric&appid=" + API_KEY;
    }

    @Override
    protected void onPostExecute(List<Weather> weathers) {
        LogUtil.task("Finished weather downloading.");
    }
}
