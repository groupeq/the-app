package pl.pzjapp.project.utils;

import java.io.IOException;
import java.util.Properties;

public class AppUtils {
    private static final String FILE_PATH = "config.properties";
    private Properties properties;

    public AppUtils(Properties properties) {
        this.properties = properties;
    }

    /**
     * @param propertyName
     * @return
     */
    public String get(String propertyName) {
        return properties.getProperty(propertyName);
    }

    /**
     * Fabricate method for getting standard test config.
     *
     * @return
     */
    public static AppUtils fromTestConfigFile() {
        return new AppUtils(readProperties(FILE_PATH));
    }

    public static Properties readProperties(String filePath) {
        Properties propertiesFromFile = new Properties();
        try {
            ClassLoader classLoaderForUtils = AppUtils.class.getClassLoader();
            if (classLoaderForUtils != null)
                propertiesFromFile.load(classLoaderForUtils.getResourceAsStream(filePath));
        } catch (IOException e) {
            throw new IllegalStateException("Cannot read test configuration file. Error: " + e.getMessage(), e);
        }
        LogUtil.debug(System.getProperties().toString());
        propertiesFromFile.putAll(System.getProperties());
        return propertiesFromFile;
    }
}


