package pl.pzjapp.project.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import pl.pzjapp.project.persistence.model.City;

public class JsonFileImporter {
    private static final String CITY_JSON_FILE = "city_list.json";

    public static List<City> getCityDataFromJsonFile() {
        String jsonFile = createJsonFile().toString();
        try {
            JSONArray jsonArray = new JSONArray(jsonFile);
            return createCitiesFromJson(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static StringBuilder createJsonFile() {
        InputStream is = loadJsonFile();
        StringBuilder stringBuilder = new StringBuilder();
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            Log.i("Error", "Could not find file: msg" + e.getMessage());
        }
        return stringBuilder;
    }

    private static InputStream loadJsonFile() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResourceAsStream(CITY_JSON_FILE);
    }

    private static List<City> createCitiesFromJson(JSONArray jsonArray) throws JSONException {
        List<City> cities = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); ++i) {
            City city = new City();
            city.setCityName(jsonArray.getJSONObject(i).getString("name"));
            city.setCountry(jsonArray.getJSONObject(i).getString("country"));
            city.setCityId(jsonArray.getJSONObject(i).getInt("id"));
            city.setFavouriteId(1);
            cities.add(city);
        }
        LogUtil.debug("Number of cities created from json" + cities.size());
        return cities;
    }
}