package pl.pzjapp.project.utils;

import android.util.Log;

public class LogUtil {

    public static void warn(String message) {
        Log.w("WARN", message);
    }

    public static void task(String message, Object... args) {
        Log.w("TASK", String.format(message, args));
    }

    public static void info(String message, Object... args) {
        Log.i("INFO", String.format(message, args));
    }

    public static void debug(String message, Object... args) {
        Log.d("DEBUG", String.format(message, args));
    }

    public static void error(String message, Object... args) {
        Log.e("ERROR", String.format(message, args));
    }
}
