package pl.pzjapp.project.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.persistence.repository.WeatherRepository;

public class WeatherViewModel extends AndroidViewModel {

    private WeatherRepository weatherRepository;

    public WeatherViewModel(@NonNull Application application) {
        super(application);
        weatherRepository = new WeatherRepository(application);
    }

    public void insertWeatherForCity(long cityId, List<Weather> weather) {
        weatherRepository.insertWeather(cityId, weather);
    }

    public LiveData<List<Weather>> getWeathers() {
        return weatherRepository.getLiveDataWeathers();
    }

//    public LiveData<List<Weather>> getWeathersForCityByName(String name) {
//        return weatherRepository.getWeathersForCityByName(name);
//    }

}
