package pl.pzjapp.project.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import pl.pzjapp.project.persistence.model.Favourite;
import pl.pzjapp.project.persistence.repository.FavouriteRepository;

public class FavouriteViewModel extends AndroidViewModel {

    private FavouriteRepository favouriteRepository;

    public FavouriteViewModel(@NonNull Application application) {
        super(application);
        this.favouriteRepository = new FavouriteRepository(application);
    }

    public LiveData<List<Favourite>> getCitiesByFavouriteId() {
        return favouriteRepository.getAllFavourites();
    }

}
