package pl.pzjapp.project.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.repository.CityRepository;

public class CityViewModel extends AndroidViewModel {
    private CityRepository cityRepository;

    public CityViewModel(@NonNull Application application) {
        super(application);
        cityRepository = new CityRepository(application);
    }

    public LiveData<List<City>> getAllCities() {
        return cityRepository.getAllCities();
    }

    public LiveData<List<City>> getCitiesForPattern(String pattern) {
        return cityRepository.getAllCitiesForPattern(pattern);
    }

    public LiveData<City> getCityById(long cityID) {
        return cityRepository.getCityById(cityID);
    }

    public LiveData<List<City>> getCitiesForFavourite(long favouriteId) {
        return cityRepository.getCitiesForFavourite(favouriteId);

    }

    public LiveData<City> getCityByName(String cityName) {
        return cityRepository.getCityByName(cityName);
    }
}
