package pl.pzjapp.project.model.tool;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Weather;

public class ModelCreator {

    public City createCity(JSONObject jsonObject) throws JSONException {
        City city = new City();
        JSONObject cityFromJSON = jsonObject.getJSONObject("city");
        city.setCityId(cityFromJSON.getInt("id"));
        city.setCountry(cityFromJSON.getString("country"));
        city.setCityName(cityFromJSON.getString("name"));
        Log.d("DEBUG", city.toString());
        return city;
    }

    public List<Weather> createWeather(JSONObject jsonObject, Long id) throws JSONException {
        List<Weather> weatherList = new ArrayList<>();
        JSONArray weatherData = jsonObject.getJSONArray("list");
        for (int i = 0; i < weatherData.length(); ++i) {
            final JSONObject firstJsonObject = weatherData.getJSONObject(i);
            final JSONArray weatherJsonArray = firstJsonObject.getJSONArray("weather");
            final JSONObject mainObject = firstJsonObject.getJSONObject("main");

            Weather weather = new Weather();
            weather.setWeatherId(weatherJsonArray.getJSONObject(0).getString("id"));
            weather.setDate(firstJsonObject.getString("dt_txt"));
            weather.setType(weatherJsonArray.getJSONObject(0).getString("main"));
            weather.setIconRef(weatherJsonArray.getJSONObject(0).getString("icon"));
            weather.setDescription(weatherJsonArray.getJSONObject(0).getString("description"));
            weather.setTemperature(mainObject.getLong("temp"));
            weather.setHumidity(mainObject.getInt("humidity"));
            weather.setPressure(mainObject.getLong("pressure"));
            weather.setWindSpeed(firstJsonObject.getJSONObject("wind").getLong("speed"));
            weather.setDt(firstJsonObject.getLong("dt"));
            weather.setCityId(id);
            weatherList.add(weather);
        }
        return weatherList;
    }

}
