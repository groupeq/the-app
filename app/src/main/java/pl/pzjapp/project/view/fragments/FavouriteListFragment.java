package pl.pzjapp.project.view.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import pl.pzjapp.project.R;
import pl.pzjapp.project.utils.WeatherDownloader;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.persistence.repository.WeatherRepository;
import pl.pzjapp.project.viewmodel.CityViewModel;
import pl.pzjapp.project.viewmodel.FavouriteViewModel;
import pl.pzjapp.project.viewmodel.WeatherViewModel;

public class FavouriteListFragment extends Fragment {

    private CityViewModel cityViewModel;
    private FavouriteViewModel favouriteViewModel;
    private WeatherViewModel weatherViewModel;
    private WeatherRepository weatherRepository ;
    private WeatherDownloader weatherDownloader ;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        weatherRepository = new WeatherRepository(getContext());
        weatherDownloader = new WeatherDownloader();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_other, container, false);
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel.class);
        weatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        favouriteViewModel = ViewModelProviders.of(this).get(FavouriteViewModel.class);
        long cityId = 798544L;
        AsyncTask<Long, Void, List<Weather>> execute = weatherDownloader.execute(cityId);
        List<Weather> weatherList = new ArrayList<>();
        try {
            weatherList = execute.get(120, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        weatherRepository.insertWeather(cityId, weatherList);

        return view;
    }
}
