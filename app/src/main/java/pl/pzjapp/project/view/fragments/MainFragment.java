package pl.pzjapp.project.view.fragments;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import pl.pzjapp.project.R;
import pl.pzjapp.project.persistence.model.City;
import pl.pzjapp.project.persistence.model.Weather;
import pl.pzjapp.project.persistence.repository.CityRepository;
import pl.pzjapp.project.persistence.repository.FavouriteRepository;
import pl.pzjapp.project.persistence.repository.WeatherRepository;
import pl.pzjapp.project.utils.WeatherDownloader;
import pl.pzjapp.project.view.WeatherStatusAdapter;
import pl.pzjapp.project.viewmodel.CityViewModel;
import pl.pzjapp.project.viewmodel.FavouriteViewModel;
import pl.pzjapp.project.viewmodel.WeatherViewModel;

public class MainFragment extends Fragment {
    private WeatherRepository weatherRepository;
    private RecyclerView rvWeatherStates;
    private ProgressBar progressBar;
    private Spinner spCities;
    private WeatherStatusAdapter adapter;
    private ArrayAdapter<String> spinnerAdapter;
    private ArrayList<Weather> listItems;
    private List<String> hints;
    private ArrayAdapter<String> atAdapter;
    private FloatingActionButton fab;
    private Context context;
    private CityViewModel cityViewModel;
    private FavouriteViewModel favouriteViewModel;
    private CityRepository cityRepository;
    private WeatherViewModel weatherViewModel;
    private FavouriteRepository favouriteRepository;
    public static String CITY_NAME;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherRepository = new WeatherRepository(getContext());
        cityRepository = new CityRepository(getContext());
        favouriteRepository = new FavouriteRepository(getContext());
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        context = getContext();
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel.class);
        favouriteViewModel = ViewModelProviders.of(this).get(FavouriteViewModel.class);
        weatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        spCities = view.findViewById(R.id.spCities);
        rvWeatherStates = view.findViewById(R.id.rvWeatherStates);
        progressBar = view.findViewById(R.id.progressBar);
        fab = getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        hints = new ArrayList<>();
        atAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, hints);
        listItems = new ArrayList<Weather>();
        populateSpinner();
        spinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spCities.setAdapter(spinnerAdapter);
        adapter = new WeatherStatusAdapter(context, listItems);
        rvWeatherStates.setHasFixedSize(true);
        rvWeatherStates.setLayoutManager(new LinearLayoutManager(context));
        rvWeatherStates.setAdapter(adapter);

        if (spinnerAdapter.getCount() > 0) {
            spCities.setSelection(0);
            fillView((String) spCities.getSelectedItem());
        } else {
            progressBar.setVisibility(View.GONE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createFormDialog(inflater);
            }
        });

        spCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String cityName = CITY_NAME = (String) spCities.getItemAtPosition(position);
                fillView(cityName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem swDisplaySpinner = menu.findItem(R.id.swShowSpinner);
        swDisplaySpinner.setVisible(true);
        Switch actionBarSwitch = menu.findItem(R.id.swShowSpinner).
                getActionView().findViewById(R.id.swToolbar);
        actionBarSwitch.setChecked(true);
        spCities.setVisibility(View.VISIBLE);
        actionBarSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spCities.setVisibility(View.VISIBLE);
                } else {
                    spCities.setVisibility(View.GONE);
                }
            }
        });
    }

    public void fillView(String cityName) {
        cityViewModel.getCityByName(cityName).observe(MainFragment.this, new Observer<City>() {
            @Override
            public void onChanged(@Nullable City city) {
                if (city != null)
                    downloadWeatherData(city.getCityId());
            }
        });
    }

    public void downloadWeatherData(long cityId) {
        this.listItems.clear();
        WeatherDownloader weatherDownloader = new WeatherDownloader();
        AsyncTask<Long, Void, List<Weather>> execute = weatherDownloader.execute(cityId);
        List<Weather> weatherList = new ArrayList<>();
        try {
            weatherList = execute.get(120, TimeUnit.SECONDS);
            System.out.println("task completed");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        for (Weather item : weatherList) {
            this.listItems.add(item);
        }

        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }


    public void createFormDialog(LayoutInflater inflater) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = inflater.inflate(R.layout.form_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog dialog = dialogBuilder.create();
        final AutoCompleteTextView atInput = dialogView.findViewById(R.id.atInput);
        Button btnSave = dialogView.findViewById(R.id.btnSave);
        atInput.setAdapter(atAdapter);
        final List<String> tmp = new ArrayList<>();
        atInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 1) {

                    cityViewModel.getCitiesForPattern(s.toString()).observe(MainFragment.this, new Observer<List<City>>() {
                        @Override
                        public void onChanged(@Nullable List<City> cityList) {
                            hints.clear();
                            for (City city : cityList) {
                                atAdapter.add(city.getCityName());
                            }
                            atAdapter.notifyDataSetChanged();
                        }
                    });

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                atInput.showDropDown();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cityName = atInput.getText().toString();
                cityViewModel.getCityByName(cityName).observe(MainFragment.this, new Observer<City>() {
                    @Override
                    public void onChanged(@Nullable City city) {
                        if (city != null) {
                            cityRepository.updateFavouriteIdForCity(city.getCityName());
                            dialog.dismiss();
                        }
                    }
                });
            }
        });
        dialog.show();
    }


    public void populateSpinner() {
        cityViewModel.getCitiesForFavourite(2).observe(this, new Observer<List<City>>() {
            @Override
            public void onChanged(@Nullable List<City> cityList) {
                spinnerAdapter.clear();
                for (City city : cityList) {
                    spinnerAdapter.add(city.getCityName());
                }
            }
        });
    }

}
